package org.ximinghui.experimental.webclient;

import lombok.Data;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.ximinghui.experimental.webclient.http.WebClient;
import org.ximinghui.experimental.webclient.json.Json;
import org.ximinghui.experimental.webclient.portedfromspring.HttpMethod;
import org.ximinghui.experimental.webclient.portedfromspring.MediaType;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * WebClient测试用例
 */
@Log4j2
class TestWebClientCase {

    // 结尾不带"/"
    final String baseUrl = "http://example.org";

    @Test
    void requestExample() {
        WebClient.ResponseSpec response;

        // URI可以和BaseUrl分开设置
        response = WebClient.create(baseUrl).get().uri("/schools/123").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // URI可以不以“/”开头，程序会自动判断并作出处理
        response = WebClient.create(baseUrl).get().uri("schools/123").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // URI支持URI变量形式
        response = WebClient.create(baseUrl).get().uri("/schools/{schoolId}", "123").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // URI支持多个URI变量形式
        response = WebClient.create(baseUrl).get().uri("/schools/{schoolId}/classes/{classId}", "123", "16").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // URI支持URI变量形式（传递Map）
        Map<String, String> params = new LinkedHashMap<>();
        params.put("schoolId", "123");
        params.put("classId", "16");
        response = WebClient.create(baseUrl).get().uri("/schools/{schoolId}/classes/{classId}", params).retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // BaseUrl可以直接给完整地址
        response = WebClient.create(baseUrl + "/schools/123").get().retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // 设置Content-Type请求头
        response = WebClient.create(baseUrl).post().contentType(MediaType.APPLICATION_JSON).body("{\"name\": \"测试\"}").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // 设置Content-Type请求头（方式二）
        response = WebClient.create(baseUrl).post().defaultHeader("Content-Type", "application/json").body("{\"name\": \"测试\"}").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // Content-Type可省略，支持自动检测（目前仅支持JSON格式）
        response = WebClient.create(baseUrl).post().body("{\"name\": \"测试\"}").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // 设置Content-Type请求头（XML格式）
        response = WebClient.create(baseUrl).post().contentType(MediaType.APPLICATION_XML).body("<name>测试</name>").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // 添加自定义标头
        response = WebClient.create(baseUrl).post().defaultHeader(
                "Content-Type", "application/json",
                "Token", "abc",
                "x-username", "tom",
                "x-age", "55"
        ).body("{\"name\": \"测试\"}").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // 通过method方法设置请求方式
        response = WebClient.create(baseUrl).method(HttpMethod.GET).uri("schools/123").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // 创建的时候不设置baseUrl，后面再设置
        response = WebClient.create().baseUrl(baseUrl).get().uri("schools/123").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // 设置为30秒超时（默认是5秒）
        response = WebClient.create(baseUrl).responseTimeout(1000 * 30).get().uri("schools/123").retrieve();
        log.info("状态码：{} 响应体：{}", response.getStatus().value(), response.getBody());

        // 将接口结果序列化为Java对象（目前仅支持JSON格式序列化）
        Dog dog = WebClient.create("http://example.org/dogs/76").get().retrieve().bodyTo(Dog.class);
        log.info("查询结果：{}", dog);

        // 将接口结果序列化为Java对象（方式二）
        dog = WebClient.create("http://example.org/dogs/76").get().retrieve().bodyTo(body -> Json.safeConvertTo(body, Dog.class));
        log.info("查询结果：{}", dog);

        // 将接口结果序列化为Java对象（XML格式可以手动转换）
        dog = WebClient.create("http://example.org/dogs/76").get().retrieve().bodyTo(body -> convertToXML(body, Dog.class));
        log.info("查询结果：{}", dog);

        // introduction是一个创意性的小东西，用于给接口起一个简单说明的名字，日志打印时会优先显示而不是接口地址
        dog = WebClient.create("http://example.org/dogs/76").get().introduction("查询狗信息").retrieve().bodyTo(body -> convertToXML(body, Dog.class));
        log.info("查询结果：{}", dog);

        // 所有示例均可不采用链式调用
        WebClient myWebClient = WebClient.create(baseUrl);
        myWebClient.uri("/students/{name}", "tom");
        myWebClient.method(HttpMethod.GET);
        myWebClient.responseTimeout(1000 * 6);
        WebClient.ResponseSpec retrieve = myWebClient.retrieve();
        log.info("查询结果：{}", retrieve.getBody());
    }

    public <T> T convertToXML(String xml, Class<T> elementClass) {
        // 实现这个方法
        return null;
    }

    @Data
    @ToString
    static class Dog {
        private String name;
        private Integer age;
    }

}
