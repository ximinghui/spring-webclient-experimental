package org.ximinghui.experimental.webclient.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Json {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static final ObjectMapper SAFE_OBJECT_MAPPER = new ObjectMapper();

    static {
        SAFE_OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * 判断字符串是否为有效的JSON格式文本
     *
     * @param string 字符串对象
     * @return 当string为有效的JSON格式文本时返回true，否则返回false
     */
    public static boolean isValid(String string) {
        if (StringUtils.isBlank(string)) return false;
        try {
            OBJECT_MAPPER.readTree(string);
        } catch (JsonProcessingException e) {
            return false;
        }
        return true;
    }

    public static <T> T safeConvertTo(String json, Class<T> elementClass) {
        try {
            return SAFE_OBJECT_MAPPER.readValue(json, elementClass);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static String minimalFormat(String json) throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(OBJECT_MAPPER.readTree(json));
    }

    public static String prettyFormat(String json) throws JsonProcessingException {
        return OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(OBJECT_MAPPER.readTree(json));
    }

}
