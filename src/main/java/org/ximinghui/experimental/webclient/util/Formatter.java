package org.ximinghui.experimental.webclient.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.ximinghui.experimental.webclient.json.Json;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Formatter {

    public static String compress(String text) {
        if (StringUtils.isBlank(text)) return "";
        if (Json.isValid(text)) {
            try {
                return Json.minimalFormat(text);
            } catch (JsonProcessingException e) {
                throw new IllegalStateException(e);
            }
        }
        if (true /* isXml() */) return text /* 压缩XML */;
        if (true /* isHtml() */) return text /* 压缩HTML */;
        return text;
    }

}
