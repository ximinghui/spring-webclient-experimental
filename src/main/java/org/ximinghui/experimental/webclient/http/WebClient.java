package org.ximinghui.experimental.webclient.http;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.ximinghui.experimental.webclient.json.Json;
import org.ximinghui.experimental.webclient.portedfromspring.HttpMethod;
import org.ximinghui.experimental.webclient.portedfromspring.HttpStatus;
import org.ximinghui.experimental.webclient.portedfromspring.MediaType;
import org.ximinghui.experimental.webclient.util.Formatter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.function.UnaryOperator;

@Log4j2
@SuppressWarnings("unused")
public class WebClient {

    protected String baseUrl;

    protected String uri;

    protected HttpMethod method;

    protected String body;

    protected int timeout = 1000 * 5;

    protected Map<String, String> headers = new LinkedHashMap<>();

    @Setter
    protected String introduction;

    public static WebClient create() {
        return new WebClient();
    }

    public static WebClient create(String baseUrl) {
        WebClient webClient = create();
        webClient.baseUrl(baseUrl);
        return webClient;
    }

    protected void setUri(String uri) {
        if (StringUtils.isBlank(uri)) return;
        if (!uri.startsWith("/")) uri = "/" + uri;
        this.uri = uri;
    }

    public WebClient baseUrl(String baseUrl) {
        Objects.requireNonNull(baseUrl);
        if (baseUrl.toLowerCase(Locale.ROOT).startsWith("https://")) {
            throw new IllegalArgumentException("暂不支持HTTPS");
        }
        if (!baseUrl.toLowerCase(Locale.ROOT).startsWith("http://")) {
            throw new IllegalArgumentException("参数baseUrl不合法：" + baseUrl);
        }
        this.baseUrl = baseUrl;
        return this;
    }

    public WebClient introduction(String introduction) {
        setIntroduction(introduction);
        return this;
    }

    public WebClient method(HttpMethod method) {
        this.method = method;
        return this;
    }

    public WebClient get() {
        method(HttpMethod.GET);
        return this;
    }

    public WebClient post() {
        method(HttpMethod.POST);
        return this;
    }

    public WebClient put() {
        method(HttpMethod.PUT);
        return this;
    }

    public WebClient patch() {
        method(HttpMethod.PATCH);
        return this;
    }

    public WebClient delete() {
        method(HttpMethod.DELETE);
        return this;
    }

    public WebClient head() {
        method(HttpMethod.HEAD);
        return this;
    }

    public WebClient options() {
        method(HttpMethod.OPTIONS);
        return this;
    }

    public WebClient uri(URI uri) {
        setUri(uri.toString());
        return this;
    }

    /**
     * 设置URI
     * <p>
     * 例:<br>
     * uri("/products/{id}", "123");<br>
     * uri("/schools/{schoolId}/classes/{classId}", "158", "16");
     *
     * @param uri 统一资源地址
     * @param uriVariables 路径变量
     * @return 当前WebClient对象
     */
    public WebClient uri(String uri, Object... uriVariables) {
        setUri(replaceUriVariables(uri, uriVariables));
        return this;
    }

    public WebClient uri(String uri, Map<String, ?> uriVariables) {
        setUri(replaceUriVariables(uri, uriVariables.entrySet()));
        return this;
    }

    public WebClient uri(String uri, Function<String, URI> uriFunction) {
        setUri(uriFunction.apply(uri).toString());
        return this;
    }

    public WebClient uri(UnaryOperator<URI> uriUnaryOperator) {
        setUri(uriUnaryOperator.apply(URI.create(this.uri)).toString());
        return this;
    }

    public WebClient defaultHeader(String... keyAndValuePair) {
        if (keyAndValuePair.length == 0) return this;
        if (keyAndValuePair.length % 2 != 0) throw new IllegalArgumentException("参数keyAndValuePair数量必须为偶数");
        for (int i = 0, size = keyAndValuePair.length; i < size; i += 2) {
            this.headers.put(keyAndValuePair[i], keyAndValuePair[i + 1]);
        }
        return this;
    }

    public WebClient contentType(MediaType contentType) {
        headers.put("Content-Type", contentType.toContentType());
        return this;
    }

    public WebClient body(String body) {
        this.body = body;
        return this;
    }

    /**
     * 设置超时
     *
     * @param timeout 以毫秒为单位指定连接超时值的int数值
     * @return 当前WebClient对象
     */
    public WebClient responseTimeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    public ResponseSpec retrieve() {
        if (StringUtils.isBlank(baseUrl)) throw new IllegalStateException("未设置baseUrl属性");
        if (method == null) throw new IllegalStateException("未设置HTTP请求方式");
        String fullUrl = baseUrl + StringUtils.defaultIfBlank(uri, "");
        HttpURLConnection connection = createHttpConnection(fullUrl);
        if (StringUtils.isNotBlank(body)) detectionContentType(body);
        for (Map.Entry<String, String> header : headers.entrySet()) {
            connection.setRequestProperty(header.getKey(), header.getValue());
        }
        try {
            connection.setRequestMethod(method.name());
            if (body != null) {
                connection.setDoOutput(true);
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(body.getBytes(StandardCharsets.UTF_8));
                outputStream.close();
            }
            log.info("请求{}接口：{} {} {}", StringUtils.defaultIfBlank(introduction, ""), method.name(), fullUrl, Formatter.compress(body));
            connection.connect();
            InputStream inputStream;
            HttpStatus status = HttpStatus.valueOf(connection.getResponseCode());
            if (StringUtils.isBlank(headers.get("Content-Type")) && status == HttpStatus.UNSUPPORTED_MEDIA_TYPE) {
                throw new IllegalStateException("请添加Content-Type请求头");
            }
            if (status.isError()) inputStream = connection.getErrorStream();
            else inputStream = connection.getInputStream();
            String responseBody = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            connection.disconnect();
            log.info("{}接口响应：{} {}", StringUtils.isBlank(introduction) ? fullUrl : introduction, status.value(), Formatter.compress(responseBody));
            return new ResponseSpec(status, responseBody);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    protected HttpURLConnection createHttpConnection(String url) {
        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        connection.setConnectTimeout(timeout);
        connection.setReadTimeout(timeout);
        return connection;
    }

    protected void detectionContentType(String body) {
        if (Json.isValid(body)) contentType(MediaType.APPLICATION_JSON);
    }

    protected String urlEncode(Object o) {
        String originalValue = o != null ? o.toString() : "";
        try {
            return URLEncoder.encode(originalValue, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    protected String replaceFirstUriVariable(String uri, Object uriVariable) {
        return uri.replaceFirst("\\{[^({)}]+}", urlEncode(uriVariable));
    }

    protected String replaceUriVariables(String uri, Object[] uriVariables) {
        if (uriVariables == null) return uri;
        for (Object uriVariable : uriVariables) uri = replaceFirstUriVariable(uri, uriVariable);
        return uri;
    }

    protected String replaceUriVariables(String uri, Collection<? extends Map.Entry<String, ?>> entries) {
        Objects.requireNonNull(entries);
        for (Map.Entry<String, ?> entry : entries) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (StringUtils.isBlank(key)) continue;
            uri = uri.replace(String.format("{%s}", key), urlEncode(value));
        }
        return uri;
    }

    @Getter
    @ToString
    @AllArgsConstructor
    public static class ResponseSpec {

        private HttpStatus status;

        private String body;

        public <T> T bodyTo(Class<T> elementClass) {
            if (Json.isValid(body)) return bodyTo(responseBody -> Json.safeConvertTo(responseBody, elementClass));
            if (1 + 1 == 2 /* isXml() */) throw new IllegalArgumentException("暂不支持XML");
            throw new IllegalArgumentException("暂不支持其它类型");
        }

        public <T> T bodyTo(Function<String, T> function) {
            return function.apply(body);
        }

    }

}
